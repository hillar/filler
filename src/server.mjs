import  { default as createDebug }  from 'debug'
const debug = createDebug('server')

//import { Worker } from 'worker_threads'
import { resolve as resolvePath, join as joinPath, dirname, basename } from 'path'
import { types as is } from 'util'
import { networkInterfaces } from 'os'
import {exec} from 'child_process'
import {get as httpget, createServer, request as httpRequest} from 'http'
import { parse as urlParse } from 'url'
import { Command } from './vendor/commander'

import { accessSync, constants, createWriteStream, renameSync, mkdirSync, unlinkSync, writeFileSync, readFileSync } from 'fs'
import { createHash } from 'crypto'

import Busboy from 'busboy'



const MAXUSEDCAPACITY = 80
const DEFAULTIP = '127.0.0.1'
const DEFAULTPORT = 7200
const RETRYTIME = 5000 // wait before retry on failed mate
// api endpoints
const CUSTERIPS = '_cat/cluster'
const CAPACITY = '_cat/capacity'
const SENDGETMATES = '_cat/mates'
const CLOSING = '_cat/cluster/leaving'
const SUBSCRIBE = '_cat/SUBSCRIBE'
const UNSUBSCRIBE = '_cat/UNSUBSCRIBE'
const NOTIFY = '_cat/NOTIFY'

function log(...args){
  console.log('INFO',JSON.stringify(args))
}
function logerror(...e){
  const message = e[0].message ? e[0].message : e
  console.log('ERROR',JSON.stringify(message))
}

const ismd5 = function (inputString) {
	return (/[a-fA-F0-9]{32}/).test(inputString);
}

function getmimetype(filename) {
  return new Promise ( (resolve) => {
    /*
    -i, --mime
        Causes the file command to output mime type strings rather than the more traditional human readable ones.  Thus it may say 'text/plain; charset=us-ascii' rather
        than ``ASCII text''.
    */

    const file = `file -i ${filename}`
    /*
    if (!canRead(filename)) {
      resolve(new Error('not exists '+filename))
      return
    }
    */
    exec(file, {stdio:['inherit','pipe','pipe']}, function(err, output, code) {
      if (!(err === null)) {
        console.error(e)
        throw err
        resolve ()
      } else if (output) {
        debug('mimetype',output)
        const m = output.split(':')[1].trim()
        const r = m ? m : 'application/octet-stream;'
        resolve(r)
      }
    })
  })
}

function getmimetypehuman(filename) {
  return new Promise ( (resolve) => {
    /*
   traditional human readable
    */

    const file = `file ${filename}`

    exec(file, {stdio:['inherit','pipe','pipe']}, function(err, output, code) {
      if (!(err === null)) {
        console.error(e)
        throw err
        resolve ()
      } else if (output) {
        debug('mimetype',output)
        const m = output.split(':')[1].trim()
        const r = m ? m : 'unknown'
        resolve(r)
      }
    })
  })
}

function df(path) {
  return new Promise ( (resolve) => {
    /*
    -k      Use 1024-byte (1-Kbyte) blocks, rather than the default.  Note that this overrides the BLOCKSIZE specification from the environment.
    -P      Use (the default) 512-byte blocks.  This is only useful as a way to override an BLOCKSIZE specification from the environment.
    */
    const df = `df -kP ${path}`
    if (!canWrite(path)) {
      resolve(new Error('not exists '+path))
      return
    }
    exec(df, {stdio:['inherit','pipe','pipe']}, function(err, output, code) {
      if (!(err === null)) {
        resolve (new Error('df ' + code))
      } else if (output) {
        /*
        'Filesystem 1024-blocks      Used Available Capacity  Mounted on',
        '/dev/disk1   243915264 188083924  55575340    78%    /',
        */
        let [filesystem,blocks,used,available,capacity,mounted ] = output.split('\n')[1].trim().replace(/\s{2,}/g, ' ').split(' ')
        if (!capacity) {
          resolve(new Error('df no capacity'))
        } else {
          try {
            capacity = Number(capacity.trim().slice(0, -1))
            resolve({filesystem,blocks,used,available,capacity,mounted})
          } catch (e) {
            resolve( new Error('df capacity not a number '))
          }
        }
      } else {
        resolve( new Error('df unknown'))
      }
    })
  })
}

async function getWriteDirs(paths,maxused=MAXUSEDCAPACITY ){
  const writeDirs = []
  for (const path of paths) {
    const {filesystem,blocks,used,available,capacity,mounted} = await df(path)
    debug('getWriteDirs',path,filesystem,blocks,used,available,capacity,mounted)
    if (Number.isInteger(capacity)) {
      if (capacity < maxused ) writeDirs.push({path,capacity,filesystem,blocks,used,available,mounted})
    } else {
      logerror('capacity not a number',{filesystem,blocks,used,available,capacity,mounted})
    }
  }
  return writeDirs.sort((a, b) => { return a.capacity - b.capacity });
}

async function getJson(ip,port,path){
  return new Promise( resolve => {
    const name = path.split('/').pop()
    const url = `http://${ip}:${port}/${path}`
    //log(url)
    httpget(url, res => {
      const { statusCode } = res;
      if (statusCode !== 200) {
          resolve({statusCode,ip,port})
      } else {
        res.setEncoding('utf8')
        let rawData = ''
        res.on('data', (chunk) => { rawData += chunk; })
        res.on('end', () => {
          try {
            const parsedData = JSON.parse(rawData)
            parsedData.ip = ip
            parsedData.port = port
            resolve(parsedData)
          } catch (e) {
            console.error(e)
            resolve({error:`${e.message}`,ip,port})
          }
        })
      }
    })
    .on('error', (e) => {
      console.error(e)
      resolve({error:`${e.message}`,ip,port})
    })
  })
}

async function getMateCapacity(ip,port){
  return await getJson(ip,port,CAPACITY)
}

async function getAllMatesCapacity(cluster,self){
  debug('getAllMatesCapacity',self,cluster)
  const domates = []
  for (const m of Object.keys(cluster)) {
    if (m===self) continue
    debug('getAllMatesCapacity',m,cluster[m],cluster)
    if (cluster[m] && !(cluster[m].readonly)) {
      const ip = m.split(':')[0]
      const port = Number(m.split(':')[1])
      domates.push(getMateCapacity(ip,port))
    }
  }
  const capas = await Promise.all(domates)
  // update cluster
  for (const m of capas.flat()) {
    if (!m.capacity) {
      if (m.error) {
        log('CLUSTER lost',m.ip,m.port)
        cluster[m.ip+':'+m.port].up = false
      }
      if (m.statusCode === 507) {
        log('MATE readonly',m.ip,m.port)
        cluster[m.ip+':'+m.port].readonly = true
      }
    }
  }
  const ret = capas.flat().filter(Boolean).filter(c => c.capacity).sort((a,b) => {return a.capacity - b.capacity})
  return ret
}

function sanitizeCliMates(mates,selfip,selfport){
  const selfips = []
  if (selfip === '0.0.0.0') {
    const interfaces = networkInterfaces()
    for (const i of Object.keys(interfaces)) {
      for ( const a of interfaces[i]) {
          selfips.push(a.address)
      }
    }
  } else if (selfip === 'localhost') {
    selfips.push('127.0.0.1')
  } else selfips.push(selfip)
  selfips.push('localhost') // HACK for mate.hostname === localhost
  return mates.filter(m => (!(m.port === selfport && selfips.includes(m.hostname) )))
}

// --------



function canRead(filename){
  try {
   accessSync(filename, constants.R_OK)
  } catch (err) {
    debug('NO READ',filename)
    return false
  }
  debug('OK READ',filename)
  return true
}

function canWrite(filename){
  try {
   accessSync(filename, constants.W_OK)
  } catch (err) {
    debug('NO WRITE',filename)
    return false
  }
  debug('OK WRITE',filename)
  return true
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

///




async function _tell2Mates (mates, tell, subscriptions, method='SUBSCRIBE', server) {
  const domates = []
  //log('MATES telling', method, mates, tell)
  for (const mate of mates) {
    domates.push( new Promise( resolve => {
      log('MATE call',method, mate.hostname+':'+mate.port,tell)
      const options = {
        hostname: mate.hostname,
        port: mate.port,
        method: method,
        headers: {'x-mate-port':server.address().port,
                  'x-mates':JSON.stringify({mates:tell})}
      }
      const req = httpRequest(options, (res) => {
          try {
            const parsedData = JSON.parse(res.headers['x-mates'])
            parsedData.from = mate
            resolve(parsedData)
          } catch (e) {
            resolve({error:`${e.message}`,from:mate})
          }
      })
      req.on('error', e => {
        resolve({error:`${e.message}`,from:mate})
      })
      req.end()
    }))
  }
  const initalanswers = await Promise.all(domates)
  for (const s of initalanswers.flat()){
    if (!s.subscriptions) {
      log ('MATE down',s)
      setTimeout(async function () {
        await _tell2Mates ([s.from], tell, subscriptions, method, server)
      }, RETRYTIME)
      continue
    }
    for (const ss of Object.keys(s.subscriptions)) {
      if (!subscriptions[ss]) {
        log('MATE from',s.from.hostname+':'+s.from.port,method,ss)
        subscriptions[ss] = s.subscriptions[ss]
      }
    }
  }
}

/// --------------

function uniq (n,o){
  if (typeof o !== 'object') return n === o ? undefined : o
  // look into arrays
  if (Array.isArray(o) && o.length > 0 ) {
    const a = []
    for (const i in o) {
      const v = uniq(o[i],n[i])
      if (v) a.push(v)
    }
    return a.length > 0 ? a : undefined
  }
  if (Object.keys(o).length === 0 ) return
  const ret = {}
  for (const k of Object.keys(o)) {
    const v = uniq(o[k],n[k])
    if (v) ret[k] = v
  }
  debug(ret)
  return Object.keys(ret).length === 0 ? undefined : ret
}



// --- main ----

(async () => {
    try {
        //params
        // TODO better way to check dirs ...
        const rresolvePath = function (p) {
          const r = resolvePath(p)
          if (canRead(r)) return r
          else throw new Error('not readable ',p)
        }
        const cliParams = new Command()
        cliParams
           .version('0.0.0')
           .usage('[options]')
           .option('--paths <list>',`list of comma separated paths to write files`, list => list.split(',').map(p => rresolvePath(p)))
           .option('--ip [ip address]', 'set ip to listen',DEFAULTIP)
           .option('--port [port number]', 'set port to listen', Number, DEFAULTPORT)
           .option('--notify [socket|url]', 'set socket|url to notify on new|updated files',urlParse)
           .option('--mates [list]','list of comma separated host & port pairs separated by colon (sample 127.0.0.1:7300,localhost:7400)', list => list.split(',').map(m=>m={hostname:m.split(':')[0],port:Number(m.split(':')[1])}),[])

        cliParams.parse(process.argv)
        //debug(cliParams.notify)
        let notify = function(){}

        function saveLocalMetaFile(metaFilename,meta){
          if (!Array.isArray(meta)) meta = [ meta ]
          if (canRead(metaFilename)){
            debug('patch old',metaFilename)
            let old = JSON.parse(readFileSync(metaFilename))
            debug('old',old[0])
            if (!Array.isArray(old)) old = [ old ]
            //old[0] = uniq(meta[0],old[0])
            //debug('diff',old[0])
            for (const o of old) meta.push(o)
          }
          writeFileSync(metaFilename,JSON.stringify(meta))
          notify(metaFilename)
        }
        /*
        TODO
        if (cliParams.notify) {
          //if (cliParams.notify.protocol ===  )
          notify = function(o) {
            debug('notify',cliParams.notify,o)
          }
        }
        */
        // set 'globals'
        const subscriptions = {}
        let self // will be set, if listening
        let dirs
        let readonly = false
        //let ppworker



        const server = createServer( async (req, res) => {
          debug('req',req.method, req.socket.remoteAddress,req.url)

          if (!req.headers['x-req-socket-remoteaddress']) req.headers['x-req-socket-remoteaddress'] = req.socket.remoteAddress

          // ------------------------
          if (req.method === 'HEAD'){
            let found = false
            for (const dir of dirs) {
              if (canRead(joinPath(dir.path,decodeURIComponent(req.url))))  {
                res.writeHead(200)
                res.end()
                found = true
                log('HEAD found',req.url)
                continue
              }
            }
            if (found === false) {
              if (!(req.headers['x-mate'])) {// ask mates
                log('HEAD ask mates', req.url)
                const domates = []
                for (const m of Object.keys(subscriptions)){
                  if (m === self) continue
                  debug('domate',self,m,subscriptions[m])
                  const mate = {}
                  mate.ip = m.split(':')[0]
                  mate.port = m.split(':')[1]
                  domates.push(new Promise( resolve => {
                    const options = {
                      hostname: mate.ip,
                      port: mate.port,
                      path: req.url,
                      method: 'HEAD',
                      headers: {'x-mate': self }
                    }
                    const reqHead = httpRequest(options, r => {
                      resolve({from:subscriptions[m],statusCode:r.statusCode})
                    })
                    reqHead.end()
                  }))
                }
                const answer = await Promise.all(domates)
                debug('HEAD domates answer',answer.flat())
                for (const a of answer.flat()) {
                   debug('aaaaa',a)
                   if (a.statusCode === 200) {
                     found = true
                     debug('HEAD found',a)
                     continue
                   }
                }
                if (found) {
                  res.writeHead(200)
                  res.end()
                } else {
                  res.writeHead(404)
                  res.end()
                }

              } else {
                res.writeHead(404)
                res.end()
              }
            }


          // ------------------------------
          } else if (req.method === 'GET'){
            if (req.url.startsWith(`/${CUSTERIPS}`)) {
              log(`GET /${CUSTERIPS}`,req.headers)
              const matesusage = await getAllMatesCapacity(subscriptions,self)
              res.end(JSON.stringify({subscriptions,matesusage}))
            } else if (req.url.startsWith(`/${CAPACITY}`)){
              log(`GET /${CAPACITY}`,req.headers)
              if (readonly === true ) {
                res.writeHead(507) //507 Insufficient Storage (WebDAV; RFC 4918)
                res.end()
              } else {
                const dirs = await getWriteDirs(cliParams.paths)
                if (dirs.length > 0) {
                  res.end(JSON.stringify({capacity:dirs[0].capacity,dirs}))
                } else {
                  readonly = true
                  res.writeHead(507) //507 Insufficient Storage (WebDAV; RFC 4918)
                  res.end()
                }
              }
            } else {
              debug(req.socket.remoteAddress,req.url)
              res.writeHead(404)
              res.end()
            }

          // --------------------------------
          } else if ((req.method === 'PUT')) {

            debug('PUT',req.url,readonly, req.headers)
            // accept only multipart/form-data
            if (req.headers['content-type'] && req.headers['content-type'].startsWith('multipart/form-data')) {
            if (readonly === true) {
               // find mate with most free space
               const matesusage = await getAllMatesCapacity(subscriptions,self)
               //log(cluster)
               if (matesusage.length > 0) {
                  log('proxy to',matesusage[0])
                // proxy to mate
                  req.pause()
                  const headers = {}
                  headers['x-req-socket-remoteaddress'] = req.socket.remoteAddress
                  headers['x-mate'] = cliParams.ip+':'+cliParams.port
                  const cpheaders = ['content-length', 'connection', 'content-type','expect']
                  for (const h of cpheaders) {
                    if (req.headers[h]) {
                      headers[h] = req.headers[h]
                    }
                  }
                  const options = {
                    hostname: matesusage[0].ip,
                    port: matesusage[0].port,
                    path: req.url,
                    method: req.method,
                    headers: headers
                  }
                  const proxy = httpRequest(options, function (r) {
                    debug('proxy headers',r.headers)
                    r.on('error', (e) => {
                      debug('r error',e)
                      res.writeHead(500)
                      res.end()
                      resolve(true)
                    })
                    if (r.statusCode != 200) {
                      debug({proxyerror:{status:r.statusCode,host:options.hostname}})
                      res.writeHead(500)
                      res.end()
                    } else  {
                      for (const h of Object.keys(r.headers)) {
                        res.setHeader(h,r.headers[h])
                        if ( h === 'content-disposition' ) {
                          debug({proxy:{downloaded:r.headers[h].split('=').pop(), method:req.method,host:this.host, port:this.port, url}})
                        }
                      }
                      r.pipe(res)
                    }
                  })
                  proxy.on('error', (e) => {
                    debug('proxy error',e)
                    res.writeHead(500)
                    res.end()
                  })
                  res.on('error', (e) => {
                    console.error(e)
                    debug('res error',e)
                    res.writeHead(500)
                    res.end()
                  })
                  res.on('finish', (e) => {
                    debug('res finish')
                  })
                  req.on('error', (e) => {
                    debug('req error',e)
                    res.writeHead(500)
                    res.end()
                  })
                  if (proxy) {
                    try {
                      req.pipe(proxy)
                    } catch (e) {
                      console.error(e)
                      logerror('PUT PROXY ERROR',e)
                      res.writeHead(500)
                      res.end()
                    }
                  }
                  req.resume()
               } else {
                 // no mates with free space
                 res.writeHead(507) //507 Insufficient Storage (WebDAV; RFC 4918)
                 res.end()
               }

            } else { // write to disk --------------------
              let remoteAddress = req.socket.remoteAddress
              let mate
              if (req.headers['x-req-socket-remoteaddress']) {
                remoteAddress = req.headers['x-req-socket-remoteaddress']
                mate = req.headers['x-mate']
              }
              log('put from',remoteAddress,mate)
              const fields = {}
              const files = []
              const ondisk = {}

              const busboy = new Busboy({ headers: req.headers })
              busboy.on('error', function(error){
                res.statusCode = 500
                res.write('busboy error: ',error.message)
                res.end()
                logerror({'busboy':{error}})
              })
              busboy.on('file', async function(fieldname, file, filename, encoding, mimetype) {

                  debug('File [' + fieldname + ']: filename: ' + filename + ', encoding: ' + encoding + ', mimetype: ' + mimetype)
                  const dirs = await getWriteDirs(cliParams.paths)
                  debug(cliParams.paths,dirs)
                  if (dirs.length === 0) {
                    logerror('DISK FULL')
                    readonly = true
                    res.writeHead(507)
                    res.end()
                    return
                  }
                  const hasher = createHash('md5')
                  //const hash = createHash('md5')
                  //const stream = await fileType.stream(file.pipe(hash))
                  //const filetype = stream.fileType
                  //log('filetype',filetype)

                  const uid = guid()
                  const spoolname = joinPath(dirs[0].path,uid)
                  //const write = createWriteStream(spoolname);
                  //file.pipe(hash).pipe(write)
	                //stream.pipe(write);
                  file.pipe(createWriteStream(spoolname));
                  file.on('data', function(data) {
                    hasher.update(data)
                  });
                  file.on('end', async function() {
                    files.push({filename})
                    ondisk.timestamp = new Date().toJSON()
                    ondisk.filename = filename
                    ondisk.contentHash = hasher.digest('hex')
                    ondisk.nameHash = createHash('md5').update(filename).digest("hex")
                    ondisk['content-type'] = await getmimetype(spoolname)
                    ondisk['content-type-human'] = await getmimetypehuman(spoolname)

                    // save meta to file named by namehash
                    let meta = {timestamp:new Date().toJSON(),ondisk,fields,headers:req.headers}
                    delete meta.headers['content-type'] // 'multipart/form-data;
                    const metaurl = ondisk.contentHash+'/'+ondisk.nameHash+'.json'
                    const metaFilename = joinPath(dirs[0].path,ondisk.contentHash,ondisk.nameHash+'.json')

                    if (canWrite(joinPath(dirs[0].path,ondisk.contentHash))) {
                      if (canWrite(joinPath(dirs[0].path,ondisk.contentHash,ondisk.contentHash))) {
                        // remove spoolfile
                        log('FILE local exists',ondisk.contentHash)
                        unlinkSync(spoolname)
                      } else {
                        // move spoolfile
                        renameSync(spoolname,joinPath(dirs[0].path,ondisk.contentHash,ondisk.contentHash))
                        notify(joinPath(dirs[0].path,ondisk.contentHash,ondisk.contentHash))
                      }
                      saveLocalMetaFile(metaFilename,meta)
                    } else {
                      // check mates
                      const domates = []
                      for (const m of Object.keys(subscriptions).filter(m=>m!==self)) {
                        const mate = subscriptions[m]
                        domates.push( new Promise( resolve => {
                          const options = {
                            hostname: mate.hostname,
                            port: mate.port,
                            method: 'HEAD',
                            path:`/${ondisk.contentHash}`,
                            headers: {'x-mate': self }
                          }
                          const req = httpRequest(options, (res) => {
                              resolve({statusCode:res.statusCode,from:mate})
                          })
                          req.on('error', e => {
                            resolve({error:`${e.message}`,from:mate})
                          })
                          req.end()
                        }))
                      }
                      let found = false
                      let patchmate
                      const exists = await Promise.all(domates)
                      for (const e of exists.flat()){
                        if (e.statusCode === 200) {
                          found = true
                          patchmate = e.from
                          continue
                        }
                      }
                      if (found) {
                        unlinkSync(spoolname)
                        log('OLD FILE, patcing',patchmate.hostname+':'+patchmate.port)
                        const p = await new Promise( resolve => {
                          const options = {
                            hostname: patchmate.hostname,
                            port: patchmate.port,
                            method: 'PATCH',
                            path:`/${metaurl}`,
                            headers: {'x-mate': self }
                          }
                          const req = httpRequest(options, (res) => {
                              resolve({statusCode:res.statusCode,from:patchmate})
                              if (res.statusCode === 200) log('OLD FILE patch ok',metaurl,patchmate)
                              else logerror('OLD FILE patch failed status',res.statusCode,metaurl,patchmate)
                          })
                          req.on('error', e => {
                            logerror('OLD FILE patch failed error',e,patchmate)
                            resolve({error:`${e.message}`,from:patchmate})
                          })
                          req.end(JSON.stringify(meta))
                        })
                        debug('p',p)


                      } else {
                      // nobody has it, save it here
                      log('FILE local new',ondisk.contentHash)
                      // make dir
                      try {
                        mkdirSync(joinPath(dirs[0].path,ondisk.contentHash))
                      } catch (e) {
                        console.dir(e)
                        throw e
                      }
                      // move spoolfile
                      if (canWrite(joinPath(dirs[0].path,ondisk.contentHash))) {
                        try {
                          renameSync(spoolname,joinPath(dirs[0].path,ondisk.contentHash,ondisk.contentHash))
                          saveLocalMetaFile(metaFilename,meta)
                          notify(joinPath(dirs[0].path,ondisk.contentHash,ondisk.contentHash))
                          // Notify workers
                          // ppworker.postMessage({new:{path:dirs[0].path,hash:ondisk.contentHash}})
                        } catch (e) {
                          console.dir(e)
                          throw e
                        }
                      } else {
                        throw new Error('failed to create direcory ',joinPath(dirs[0].path,ondisk.contentHash))
                      }
                      }
                    }

                  });
                });
              busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
                if (val) {
                  try {
                    val = JSON.parse(val)
                  } catch (e) {
                    //noops
                  }
                  fields[fieldname] = val
                }
              })
              busboy.on('finish', async function() {
                //fields.headers = req.headers
                // TODO clean headers
                //fields.ondiskTimestamp = Date.now()
                //debug('busboy finish',files,fields,req.url)
                // PUT form only, no files
                if (files.length === 0) {
                  const bittes = req.url.replace('//','/').split('/')
                  const contentHash = bittes[1]
                  const nameHash = bittes[2] ? bittes[2].split('.')[0] : contentHash
                  if (ismd5(contentHash)){
                     if (ismd5(nameHash)){
                       //---------- start PATCH FORM
                       debug('no file, just form',contentHash,nameHash,fields)
                       // check mates
                       const metaurl = contentHash+'/'+nameHash+'.json'
                       const domates = []
                       for (const m of Object.keys(subscriptions)) {
                         const mate = subscriptions[m]
                         domates.push( new Promise( resolve => {
                           const options = {
                             hostname: mate.hostname,
                             port: mate.port,
                             method: 'HEAD',
                             path:`/${contentHash}`,
                             headers: {'x-mate': self }
                           }
                           const req = httpRequest(options, (res) => {
                               //debug('patch form',mate,options)
                               resolve({statusCode:res.statusCode,from:mate})
                           })
                           req.on('error', e => {
                             resolve({error:`${e.message}`,from:mate})
                           })
                           req.end()
                         }))
                       }
                       let found = false
                       let patchmate
                       const exists = await Promise.all(domates)
                       for (const e of exists.flat()){
                         if (e.statusCode === 200) {
                           found = true
                           patchmate = e.from
                           continue
                         }
                       }
                       if (found) {
                         //unlinkSync(spoolname)
                         log('PUT FORM, patcing',patchmate.hostname+':'+patchmate.port)
                         const p = await new Promise( resolve => {
                           const options = {
                             hostname: patchmate.hostname,
                             port: patchmate.port,
                             method: 'PATCH',
                             path:`/${contentHash}/${nameHash}.json`,
                             headers: {'x-mate': self }
                           }
                           const req = httpRequest(options, (res) => {
                               resolve({statusCode:res.statusCode,from:patchmate})
                               if (res.statusCode === 200) log('PUT FORM patch ok',metaurl,patchmate)
                               else logerror('PUT FORM patch failed status',res.statusCode,metaurl,patchmate)
                           })
                           req.on('error', e => {
                             logerror('PUT FORM patch failed error',e,patchmate)
                             resolve({error:`${e.message}`,from:patchmate})
                           })
                           let meta = {timestamp:new Date().toJSON(),ondisk:{contentHash,nameHash},fields,headers:req.headers}
                           req.end(JSON.stringify(meta))
                         })
                         debug('p',p)
                         res.end()
                       } else {
                         res.writeHead(404)
                         res.end
                       }
                       //---------- end PATCH FORM

                     } else {
                       res.writeHead(415)
                       res.end()
                     }
                  } else {
                    res.writeHead(415)
                    res.end()
                  }

                } else {
                  debug('busboy finish',ondisk)
                  res.writeHead(200,{saved:`http://${cliParams.ip}:${cliParams.port}/${ondisk.contentHash}`})
                  res.end()
                }
              });
              req.pipe(busboy);
            }
          } else {
            res.writeHead('415')
            res.end()
          }

          // ---------------------------------
          } else if (req.method === 'PATCH') {
            let found = false
            for (const dir of dirs) {
              let _url = decodeURIComponent(req.url)
              if ( basename(dirname(_url)) === '' ) {
                _url = joinPath(_url,_url+'.json')
                debug('PATCH no name',req.url, _url)
              }
              const _dirname = dirname(_url)
              const _path = joinPath(dir.path,_dirname)
              if (canWrite(_path)) {
                const _basename = basename(_url)
                const _fullname = joinPath(dir.path,_dirname,_basename)
                  found = true
                  await new Promise( resolve => {
                    let rawData = ''
                    req.on('data', (chunk) => { rawData += chunk; })
                    req.on('end', async () => {
                      try {
                        let raw = JSON.parse(rawData)
                        if (!raw.fields) {
                          const tmp = raw
                          raw = {}
                          raw.fields = tmp
                        }
                        if (!raw.timestamp) raw.timestamp = new Date().toJSON()
                        if (!raw.headers ) raw.headers = req.headers
                        let patch = [ raw ]
                        saveLocalMetaFile(_fullname,raw)
                        log('PATCHed',_fullname)
                        res.writeHead(200)
                      } catch (e) {
                        //console.error(e)
                        debug(e)
                        logerror('PATCH 415',{self,status:415,host:self,url:req.url,e})
                        res.writeHead(415) // Unsupported Media Type
                      }
                      res.end()
                      resolve()
                    })
                    req.on('error', (e) => {
                      logerror('PATCH error',e)
                      resolve()
                    })
                  })
              }
            }
            const _mates = Object.keys(subscriptions).filter(m=>m!==self)
            if (found === false && !(req.headers['x-mate']) && _mates.length > 0) { // ask mates
              let rawData = ''
              req.on('data', (chunk) => { rawData += chunk; })
              req.on('end', async () => {
                const domates = []
                const headers = {}
                headers['x-mate'] = self
                for (const h of Object.keys(req.headers)) {
                  headers[h] = req.headers[h]
                }
                for (const m of _mates) {
                  if (m === self) continue
                  debug('PATCH proxy',m)
                  const mate = {}
                  mate.ip = m.split(':')[0]
                  mate.port = m.split(':')[1]
                  domates.push(new Promise( resolve => {
                    const options = {
                      hostname: mate.ip,
                      port: mate.port,
                      path: req.url,
                      method: 'PATCH',
                      headers: headers
                    }
                    const reqHead = httpRequest(options, r => {
                      if (r.statusCode === 200) {
                        res.writeHead(200)
                        res.end()
                        debug('PATCH proxy ok',mate, req.url)
                        found = true
                      }
                      resolve({mate,status:r.statusCode})
                    })
                    reqHead.on('error', error => {
                      logerror('PATCH mate error', error)
                      if (cluster[m]) delete cluster[m]
                    })
                    reqHead.end(rawData)
                  }))
                }
                const results = await Promise.all(domates)
                debug('PATCH proxy results',results)
                if (results.length === 0) {
                  debug('PATCH no results from mates 404')
                  res.writeHead(404)
                  res.end()
                } else {
                  //res.write(req.url)
                  res.end()
                }
              })
            } else {
              if (!found) {
                log('PATCH 404',{self,status:404,host:self,url:req.url})
                res.writeHead(404)
              }
              res.end()
            }

          // -----------------------------------
          } else if (req.method === 'UNSUBSCRIBE') {
            log('UNSUBSCRIBE',req.socket.remoteAddress,req.headers['x-mate-port'])
            if (req.headers['x-mate-port'] && Number(req.headers['x-mate-port'])){
                const mate = req.socket.remoteAddress+':'+req.headers['x-mate-port']
               if (subscriptions[mate]) delete subscriptions[mate]
            } else logerror('UNSUBSCRIBE bad headers',req.socket.remoteAddress,req.headers['x-mate-port'])
            res.end()

          // -----------------------------------
          } else if (req.method === 'SUBSCRIBE') {
            debug('SUBSCRIBE', req.headers)
            if (req.headers['x-mate-port'] && Number(req.headers['x-mate-port']) && req.headers['x-mates']) {
              log('SUBSCRIBE from',req.socket.remoteAddress+':'+req.headers['x-mate-port'])
              res.writeHead(200,{'x-mates':JSON.stringify({subscriptions})})
              const mate = req.socket.remoteAddress+':'+req.headers['x-mate-port']
              if (!subscriptions[mate]) {
                subscriptions[mate] = {
                  started:Date.now(),
                  up:true,
                  hostname:req.socket.remoteAddress,
                  port:Number(req.headers['x-mate-port'])
                }
                log('SUBSCRIBE added',req.socket.remoteAddress,req.headers['x-mate-port'])
                try {
                  const mates = JSON.parse(req.headers['x-mates'])
                  debug('SUBSCRIBE got',mates)
                } catch (e) {
                  logerror('NOTIFY bad json',req.socket.remoteAddress,req.headers['x-mate-port'],req.headers['x-mates'])
                }
                // NOTIFY others
                //const needsNotify = []
                for (const m of Object.keys(subscriptions)) {
                  if (subscriptions[m].hostname === server.address().address && subscriptions[m].port === server.address().port) continue
                  if (subscriptions[m].hostname === req.socket.remoteAddress && subscriptions[m].port === Number(req.headers['x-mate-port'])) continue
                  //needsNotify.push(subscriptions[m])
                  log('needsNotify',m)
                  _tell2Mates ([subscriptions[m]], subscriptions, subscriptions, 'NOTIFY',server)
                }
                //debug('needsNotify',needsNotify)
                //_tell2Mates (needsNotify, subscriptions, subscriptions, 'NOTIFY',server)

              } else {
                subscriptions[mate].up = true
                log('SUBSCRIBE',{up:subscriptions[mate]})
              }
            } else logerror('SUBSCRIBE bad headers',req.socket.remoteAddress,req.headers['x-mate-port'])
            res.end()

          // -----------------------------------
          } else if (req.method === 'NOTIFY') {
            if (req.headers['x-mate-port'] && Number(req.headers['x-mate-port']) && req.headers['x-mates']) {
              log('NOTIFY from',req.socket.remoteAddress+':'+req.headers['x-mate-port'])
              res.writeHead(200,{'x-mates':JSON.stringify({subscriptions})})
              try {
                const mates = JSON.parse(req.headers['x-mates'])
                debug('NOTIFY got',mates)
              } catch (e) {
                logerror('NOTIFY bad json',req.socket.remoteAddress,req.headers['x-mate-port'],req.headers['x-mates'])
              }

            }  else logerror('NOTIFY bad headers',req.socket.remoteAddress,req.headers['x-mate-port'],req.headers['x-mates'])

            res.end()
          } else {
            // not GET or PUT or POST nor PATCH
            debug('501', req.method, req.url,req.headers)
            res.writeHead(501) // Not Implemented
            res.end()
          }
        })

        server.on('error', (err) => {
          logerror(err)
        })

        server.on('clientError', (err, socket) => {
          socket.end('HTTP/1.1 400 Bad Request\r\n\r\n')
          logerror('clientError',err)
        })

        server.on('close', async () => {
          log(server.address(),'closing ...')
        })

        // subscribe to mates after listening is started
        server.on('listening', async () => {
          log('LISTENING',server.address())
          dirs = await getWriteDirs(cliParams.paths)
          readonly = (dirs.length === 0)
          //readonly = (cliParams.port === 7200)
          for (const d of dirs) log('WRITABLE',d)
          const initalMates = sanitizeCliMates(cliParams.mates,server.address().address,server.address().port)
          for (const m of initalMates) log('MATE inital ',m)
          await _tell2Mates (initalMates, initalMates, subscriptions, 'SUBSCRIBE',server)
          if (Object.keys(subscriptions).length === 0) log('no mates')
          self = server.address().address+':'+server.address().port
          subscriptions[self] = {hostname:server.address().address,port:server.address().port,started:Date.now()}
          /*
          const ppsource = './src/pps/pps.mjs'
          const elasticservers = [{ hostname:'localhost', port: 9200 }]
          const tikaservers = [{ hostname:'localhost', port: 9998 }]
          const erservers = [{ hostname:'localhost',port:9997 }]
          try {
          ppworker = new Worker(ppsource,{workerData:{parent:{port:cliParams.port,hostname:cliParams.ip},elasticservers,tikaservers,erservers}})
        } catch (e) {
          logerror('worker start',e)
        }
          ppworker.on('online', () => { debug('worker online',ppsource)})
          ppworker.on('error', (e) => { logerror('worker',ppsource,e)
          })
          ppworker.on('message', (m) => { log('worker message',ppsource,m)})
          ppworker.on('exit', (m) => { log('worker exit',ppsource,m)
            try {
            ppworker = new Worker(ppsource,{workerData:{parent:{port:cliParams.port,hostname:cliParams.ip},elasticservers,tikaservers,erservers}})
          } catch (e) {
            logerror('worker restart',e)
          }
          })
          */

        })

        process.on('SIGHUP',  () => {
              log('got SIGHUP')
              // TODO reload conf
        })

        process.on('SIGINT', async () => {
          if (process.gotsinint) {
            log('got another SIGINT, force exit')
            process.exit(0)
          }
          log('got SIGINT, waiting to server close')
          //notify other in cluster
          const _mates = Object.keys(subscriptions).filter(m=>m!==self).map(m=> m = {hostname:m.split(':')[0],port:m.split(':')[1]})
          debug('UNSUBSCRIBE', _mates)
          const domates = []
          for (const mate of _mates) {
            domates.push( new Promise( resolve => {
              const options = {
                hostname: mate.hostname,
                port: mate.port,
                //path: `/${UNSUBSCRIBE}?mate=${self}`,
                method: 'UNSUBSCRIBE',
                headers: {'x-mate-port':server.address().port}
              }
              const req = httpRequest(options, (res) => {
                resolve()
              })
              req.on('error', error => {
                resolve()
              })
              req.end()
            }))
          }
          await Promise.all(domates)
          server.close(() => {
              process.exit(0)
          })
          if (!process.gotsinint) process.gotsinint = true
        })

        process.on('SIGTERM', () => {
            log('got SIGTERM')
            server.close(() => {
              process.exit(0)
            })
        })
        // start http server
        server.listen(cliParams.port,cliParams.ip)
    } catch (e) {
      console.error(e)
      logerror(e)
      process.exit(-1)
    }
})()
