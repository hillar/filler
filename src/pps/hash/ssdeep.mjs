import { default as ss } from 'ssdeep'

// https://www.virusbulletin.com/virusbulletin/2015/11/optimizing-ssdeep-use-scale/

export function hash_from_file (fn) {
  return new Promise( resolve => {
    try {
      const hash = ss.hash_from_file(fn)
      resolve(hash)
    } catch (e) {
      resolve ()
    }
  })
}

export function hash_from_str (str) {
  return new Promise( resolve => {
    try {
      const hash = ss.hash(str)
      resolve(hash)
    } catch (e) {
      resolve ()
    }
  })
}

export function hash_compare (h1,h2) {
  return ss.compare(h1,h2)
}

export function hash_split (ss) {
  let [chunksize, chunk, double_chunk] = ss.split(':')
  chunksize = Number(chunksize)
  return { chunksize, chunk, double_chunk }
}

export function findSimilar (id,index,elasticserver,threshold_grade=75) {
  return new Promise( async (resolve) => {
      const result = []
      const ssdeep = await elasticGetFieldFromDoc(id,'ssdeep',index,elasticserver)
      if (ssdeep ) {
          let {chunksize, chunk, double_chunk } = hash_split(ssdeep)
          const q = `
"query": {
    "bool": {
        "must": [
            {
                "terms": {
                    "chunksize": [${chunksize}, ${chunksize * 2}, ${Math.round(chunksize / 2)}]
                }
            },
            {
                "bool": {
                    "should": [
                        {
                            "match": {
                                "chunk": {
                                    "query": ${chunk}
                                }
                            }
                        },
                        {
                            "match": {
                                "double_chunk": {
                                    "query": ${double_chunk}
                                }
                            }
                        }
                    ],
                    "minimum_should_match": 1
                }
            }
        ]
    }
}
`
        const results = await elasticQuery(q,index,elasticserver)
        for (const record of results['hits']['hits']) {
          const record_ssdeep = record['_source']['ssdeep']
          if (record_ssdeep) {
            const ssdeep_grade = compare(record_ssdeep, ssdeep)
            if (ssdeep_grade >= threshold_grade)
            result.push(record['_id'])
          }
        }


      } else {
        debug('no ssdeep',id,index)

      }
      resolve(result)
  })
}

// curl -s -XPUT -H 'Content-Type: application/json' "http://aju2:9200/meta/_doc/1" -d'{}'
// curl -s -XPUT -H 'Content-Type: application/json' "http://aju2:9200/meta/_settings" -d '{"index.mapping.total_fields.limit":2000,"number_of_replicas":0}'

// curl -s -XPUT -H 'Content-Type: application/json' "http://aju2:9200/content/_doc/1" -d'{}'

//export const mapping =

/*
curl -s -XPUT -H 'Content-Type: application/json' "http://aju2:9200/content-127.0.0.1_7200" -d'{
  "settings": {
    "analysis": {
      "analyzer": {
        "ssdeep_analyzer": {
          "tokenizer": "ssdeep_tokenizer"
        },
        "fulltext_analyzer": {
          "type": "custom",
          "tokenizer": "whitespace",
          "filter": [
            "lowercase",
            "type_as_payload"
          ]
        }
      },
      "tokenizer": {
        "ssdeep_tokenizer": {
          "type": "ngram",
          "min_gram": 7,
          "max_gram": 7
        }
      }
    }
  },
  "mappings": {
      "dynamic": "strict",
      "properties": {
        "content": {
          "type": "text",
          "term_vector": "with_positions_offsets_payloads",
          "store" : true,
          "analyzer" : "fulltext_analyzer"
        },
        "chunksize": {
          "type": "integer"
        },
        "chunk": {
          "analyzer": "ssdeep_analyzer",
          "type": "text"
        },
        "double_chunk": {
          "analyzer": "ssdeep_analyzer",
          "type": "text"
        },
        "ssdeep": {
          "type": "keyword"
        }
      }
    }
  }
}
'
*/
