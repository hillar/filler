import { digestFile, digestString } from './hasher'
import { readFileSync, writeFileSync, unlinkSync } from 'fs'


(async () => {
      try {

          /*

          ! create a empty file

          # touch kala
          # ls -tlah kala
          -rw-r--r-- 1 root root 0 Mar 29 09:25 kala

          # cat kala | md5sum | awk '{print $1}'
          d41d8cd98f00b204e9800998ecf8427e

          ! The echo command appends a new line at the end, by default.
          ! The -n option omits this character.

          echo -n 'kala' | md5sum | awk '{print $1}'
          19dd1947a95454ccaf223a731c32db0c

          # echo -n 'kala' > kala

          # md5sum kala
          19dd1947a95454ccaf223a731c32db0c  kala

          # cat kala | md5sum | awk '{print $1}'
          19dd1947a95454ccaf223a731c32db0c



          */

          const emptyS = digestString('')

          if ( emptyS === 'd41d8cd98f00b204e9800998ecf8427e') {

          } else {
            throw new Error('emtpy string digest is not d41d8cd98f00b204e9800998ecf8427e')
          }


          const kala = digestString('kala')

          if ( kala === '19dd1947a95454ccaf223a731c32db0c') {

          } else {
            throw new Error('digest of string "kala" is not 19dd1947a95454ccaf223a731c32db0c : ' + kala)
          }

          /*

          content of empty.file is md5sum of empty input

          # echo -n | md5sum | echo -n $(awk '{print $1}') > empty.file2
          # cat empty.file2
          d41d8cd98f00b204e9800998ecf8427e

          # md5sum empty.file2
          74be16979710d4c4e7c6647856088456  empty.file2

          */

          writeFileSync('empty.file', Buffer.from(digestString(''), 'utf8') )

          const empty = await digestFile('empty.file')

          unlinkSync('empty.file')

          if (empty === '74be16979710d4c4e7c6647856088456') {
            // NOOP
          } else {
            throw new Error('digest of file of emtpty digest is not 74be16979710d4c4e7c6647856088456 : ', empty)
          }

          const notexist = await digestFile('notexist.file')
          if (notexist === undefined) {
            // NOOP
          } else {
            throw new Error('not existing file md5 digest is not undefined')
          }

      } catch (e) {
        console.error(e)
        process.exit(-1)
      }
  })();
