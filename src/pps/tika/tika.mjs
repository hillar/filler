import  { default as createDebug }  from 'debug'
const debug = createDebug('tika')

import { request } from 'http'
import { createReadStream } from 'fs'

const TIKASERVER = 'localhost'
const TIKAPORT = 9998
const TIKASERVERS = [{hostname:TIKASERVER,port:TIKAPORT}]
const TIKAMAXSIZE = 314572800 // 300 Mb

const renameKeys = (keysMap, obj) => Object
    .keys(obj)
    .reduce((acc, key) => ({
        ...acc,
        ...{ [keysMap[key] || key]: obj[key] }
    }), {})



function getRandomI(max) {
    return Math.floor(Math.random() * (max + 1))
}

export async function tikaRMeta(filename, tikaservers=TIKASERVERS) {
  const i = getRandomI(tikaservers.length-1)
  const hostname = tikaservers[i].hostname
  const port = tikaservers[i].port
  const r = await _tikaRMeta(filename, hostname, port)
  if (r === false) {
    debug('DOWN',hostname, port)
    if (tikaservers.length > 1) {
      const tmp = [ ...tikaservers]
      tmp.splice(i,1)
      debug('retry',tmp)
      return tikaRMeta(filename,tmp)
    } else {
      console.error('no live tika servers');
      process.exit(-1)
    }
  }
  return r === false ? {} : r
}

function _tikaRMeta(filename, hostname,port) {
  debug('---- ask tika',hostname,port)
  return new Promise ( async (resolve) => {
    const options = {
      hostname: hostname,
      port: port,
      path: '/rmeta/text',
      method: 'PUT',
      headers: {
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      //debug(`statusCode: ${res.statusCode}`)
      let data = ''//[]
      const body = []
      let counter = 0
      let total = 0
      res.on('data', (chunk) => {
        //console.dir(''+d)
        counter ++
        total += chunk.length
        /*
        if (total % (1024 * 1024 * 10) === 0) //debug('data',total)
        if (total > TIKAMAXSIZE) {
          body = [Buffer.from(`[{"error":"tika result is more than max ${TIKAMAXSIZE}"}]`)]
          console.error('to big',total,TIKAMAXSIZE,filename)
          resolve({})
          res.destroy()
          return
          //throw new Error('to biiiiiiiig')
        } else {
        */
        //data.push(d)
            body.push(chunk)
            //console.dir(''+chunk)

        //}
      })
      res.on('error', (e) => {
        console.error('res on error', filename)
        console.error(e)
        resolve({})
      })
      res.on('end',() => {
        let rr = {}
        let r = {}
        try {
          //console.log('',data.length)
          //debug('data',data.length,total)
          try {
            //r = JSON.parse(Buffer.concat(data).toString())
            r = JSON.parse(Buffer.concat(body).toString())
          } catch (e) {
            console.error('TIKA BAD',hostname,port,filename,counter,total,body.length,res.statusCode)
            //console.dir(Buffer.concat(body).toString())

            //console.error(Buffer.concat(data).toString())
            //console.error(e)
            //if (counter > 1) process.exit(-1)
            resolve({tikaerror:res.statusCode})
            return
          }
          //debug('r',r.length)
          if (r && r[0]) {
            rr = r.shift()
            //debug('r',Object.keys(rr))
            const regex = /\W/gi;
            const keysMap = {}
             for (const key of Object.keys(rr)){
               // rename tika fields
               if (key.includes(':') || key.includes(' ')) {
                 keysMap[key] = key.replace(regex,'-').toLowerCase()
                 // use 'content' for 'X-TIKA:content'
                 if (key==='X-TIKA:content') keysMap[key] = 'content'
               }
             }
             rr = renameKeys(keysMap, rr)
             //debug(keysMap)
             if (r.length > 0) {
               //debug('rrr',r.length)

               while (r.length > 0) {
                 let rrr = r.shift()
                 if (rrr['X-TIKA:content'] && rrr['X-TIKA:content'].length) {
                   if (!rr.attachments) rr.attachments = []
                   //debug('rrr',Object.keys(rrr))
                   const regex = /\W/gi;
                   const keysMap = {}
                    for (const key of Object.keys(rrr)){
                      // rename tika fields
                      if (key.includes(':') || key.includes(' ')) {
                        keysMap[key] = key.replace(regex,'-').toLowerCase()
                        // use 'content' for 'X-TIKA:content'
                        if (key==='X-TIKA:content') keysMap[key] = 'content'
                      }
                    }
                    rrr = renameKeys(keysMap, rrr)
                    rr.attachments.push(rrr)
                  }
               }

             }

          }
          ////debug('content length',r.content.length)

          resolve(rr)
        } catch (e) {
          //debug('catch',e)
          resolve({})
        }
      })
    })
    req.on('error', (error) => {
      //debug('req.on',error)
      resolve(false)
    })
    createReadStream(filename).pipe(req)
  })
}
