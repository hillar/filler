import  { default as createDebug }  from 'debug'
const debug = createDebug('process')

import {createHash } from 'crypto'
import { readdirSync, readFileSync, watch, createReadStream } from 'fs'
import { join } from 'path'
import { digestFile } from '../hash/hasher'
import { tikaRMeta } from '../tika/tika'
import { putDoc2Elastic, docExistInElastic, getDoc, indexExists, put2Elastic } from '../elasticsearch/elastic'
import { isEstonianIDCode } from '../parsers/est-id'
import { hash_from_str, hash_split } from '../hash/ssdeep'


const MINCONTENTLENGTH2FIND = 11
const CONTENT = 'content'
const FILES = 'files'
const META = 'meta'
const FINDINGS = 'findings'






/*

curl -XPUT 'aju2:9200/meta/_settings' -H 'Content-Type: application/json' -d'
{
"index" : {
"mapping" : {
"total_fields" : {
"limit" : "100000"
}
}
}
}'


*/



const END_OF_SEQUENCE = Symbol()

function* tokenize(chars) {
    let iterator = chars[Symbol.iterator]();
    let ch;
    do {
        ch = getNextItem(iterator);
        if (isWordChar(ch)) {
            let word = '';
            do {
                word += ch;
                ch = getNextItem(iterator);
            } while (isWordChar(ch));
            yield word;
        }
    } while (ch !== END_OF_SEQUENCE);
}

function getNextItem(iterator) {
    let item = iterator.next();
    return item.done ? END_OF_SEQUENCE : item.value;
}
function isWordChar(ch) {
    return typeof ch === 'string' && /^\S$/.test(ch);
}

function trimArray(a) {
    return [ ...a].filter( i => (!(i === undefined)) )
}

async function pp(str, ...fns){
  const r = {}
  for (const tkn of tokenize(str)){
    const pfns = []
    for (const fn of fns){
      pfns.push(new Promise( resolve => {
        resolve(fn(tkn))
      }))
    }
    const rr = trimArray(await Promise.all(pfns))
    if (rr.length > 0) {
      for (const rrr of rr){
        for (const k of Object.keys(rrr)){
          // uniq key and value(s)
          if (r[k]){
            if (Array.isArray(r[k])){
              if (!(r[k].includes(rrr[k]))) r[k].push(rrr[k])
            } else if (r[k] !== rrr[k]){
              r[k] = [r[k]]
              r[k].push(rrr[k])
            }
          } else {
            // array or single value ?
            r[k] = [rrr[k]]
          }
        }
      }
    }
  }

  return r
}

// TODO http://data.iana.org/TLD/tlds-alpha-by-domain.txt
const email = (s) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (re.test(s.toLowerCase())) return {'email':s}
}

function estID(s){
  const re = /^[1-6][0-9]{2}[0,1][0-9][0-9]{2}[0-9]{4}$/
  if (re.test(s)) {
    if (isEstonianIDCode(s)) {
      return {'estID':s}
    }
  }
}

function ip4(s){
  const re = /^(25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)$/
  if (re.test(s)) return {'ipv4':s}
}

function ip6(s){
  const re = /^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$/
  if (re.test(s)) return {'ipv6':s}
}

function iban(s){
    function likeIban(str){
      return /^([A-Z]{2}[ \-]?[0-9]{2})(?=(?:[ \-]?[A-Z0-9]){9,30}$)((?:[ \-]?[A-Z0-9]{3,5}){2,7})([ \-]?[A-Z0-9]{1,3})?$/.test(str);
    }

    function validateIbanChecksum(iban) {
      const ibanStripped = iban.replace(/[^A-Z0-9]+/gi,'').toUpperCase()
      const m = ibanStripped.match(/^([A-Z]{2})([0-9]{2})([A-Z0-9]{9,30})$/)
      if(!m) return false
      const numbericed = (m[3] + m[1] + m[2]).replace(/[A-Z]/g,function(ch){
          return (ch.charCodeAt(0)-55)
      })
      const mod97 = numbericed.match(/\d{1,7}/g).reduce(function(total, curr){ return Number(total + curr)%97},'')
      return (mod97 === 1)
    };
    if (likeIban(s)){
      if (validateIbanChecksum(s))
      return {'iban':s}
    }
}



function pprocess(str) {
  return pp(str, email,ip4,ip6,iban,estID)
}


function getMetaFiles(dir){
  const metaFiles = []
  const files = readdirSync(dir)
  for (const f of files) {
    if (f.endsWith('.json')) metaFiles.push(f)
  }
  return metaFiles
}

function flatten (target, opts) {
  opts = opts || {}

  var delimiter = opts.delimiter || '.'
  var maxDepth = opts.maxDepth
  var output = {}

  function step (object, prev, currentDepth) {
    currentDepth = currentDepth || 1
    Object.keys(object).forEach(function (key) {
      var value = object[key]
      var isarray = opts.safe && Array.isArray(value)
      var type = Object.prototype.toString.call(value)

      var isobject = (
        type === '[object Object]' ||
        type === '[object Array]'
      )

      var newKey = prev
        ? prev + delimiter + key
        : key

      if (!isarray  && isobject && Object.keys(value).length &&
        (!opts.maxDepth || currentDepth < maxDepth)) {
        return step(value, newKey, currentDepth + 1)
      }

      output[newKey] = value
    })
  }

  step(target)

  return output
}


function cleanup(obj){
  //debug('cleanup',key,level, typeof obj)
  if (typeof obj !== 'object') return obj
  // look into arrays
  if (Array.isArray(obj) && obj.length > 0 ) {
    const a = []
    for (const o of obj) {
      const v = cleanup(o)
      if (v) a.push(v)
    }
    if (a.length > 0) {
      if (typeof a[0] === 'object') {
        const r = {}
        const keys = Object.keys(a[0])
        for (const item of a) {
          for (const key of keys){
            if (typeof item[key] === 'object'){
              r[key] = flatten(item[key])
              /*
                debug('cleanup aaaaaaa',level,key, item[key])
                const ks = Object.keys(item[key])
                for (const k of ks) {
                  if (!r[key+'.'+k]) r[key+'.'+k] = []
                  r[key+'.'+k].push(item[key][k])
                }
                */

            } else {
              if (!r[key]) r[key] = []
              r[key].push(item[key])
            }
          }
        }
        return r
      } else return a
    } else return
  }
  if (Object.keys(obj).length === 0 ) return
  const ret = {}
  for (const o of Object.keys(obj)) {
    const v = cleanup(obj[o])
    if (v) ret[o] = v
  }
  return ret
}

const ismd5 = function (inputString) {
	return (/[a-fA-F0-9]{32}/).test(inputString);
}

export async function processDir(contentHash,archiveDirectory, tikaservers, elasticservers,suffix='127.0.0.1_7200'){


    const CONTENTINDEX = CONTENT + '-' + suffix
    const FILESINDEX = FILES + '-' + suffix
    const METAINDEX = META + '-' + suffix
    const FINDINGSINDEX = FINDINGS + '-' + suffix
    debug('indexes',CONTENTINDEX,FILESINDEX,METAINDEX,FINDINGSINDEX)
    // check index and creatre if not exists
    //const commonSettings = {settings:{"number_of_replicas":0, "index.refresh_interval": "10s"}}
    const contentExists = await indexExists(CONTENTINDEX,elasticservers)
    if (!(contentExists === true)) {

      /*
      curl -s -XPUT -H 'Content-Type: application/json' "http://aju2:9200/content-127.0.0.1_7200" -d'{
      */
        const contentSettings = { "settings": {
          "number_of_replicas":0,
          "index.refresh_interval": "10s",
          "analysis": {
            "analyzer": {
              "ssdeep_analyzer": {
                "tokenizer": "ssdeep_tokenizer"
              },
              "fulltext_analyzer": {
                "type": "custom",
                "tokenizer": "whitespace",
                "filter": [
                  "lowercase",
                  "type_as_payload"
                ]
              }
            },
            "tokenizer": {
              "ssdeep_tokenizer": {
                "type": "ngram",
                "min_gram": 7,
                "max_gram": 7
              }
            }
          }
        },
        "mappings": {
            "dynamic": "strict",
            "properties": {
              "content": {
                "type": "text",
                "term_vector": "with_positions_offsets_payloads",
                "store" : true,
                "analyzer" : "fulltext_analyzer"
              },
              "chunksize": {
                "type": "integer"
              },
              "chunk": {
                "analyzer": "ssdeep_analyzer",
                "type": "text"
              },
              "double_chunk": {
                "analyzer": "ssdeep_analyzer",
                "type": "text"
              },
              "ssdeep": {
                "type": "keyword"
              }
            }
          }
        }
        const createdContent = await put2Elastic(JSON.stringify(contentSettings),CONTENTINDEX,elasticservers)
    }

    const filesExists = await indexExists(FILESINDEX,elasticservers)
    if (!(filesExists === true)) {
      const createdFiles = await put2Elastic(JSON.stringify({settings:{"index.mapping.ignore_malformed": true,"number_of_replicas":0, "index.refresh_interval": "10s"}}),FILESINDEX,elasticservers)

    }
    const metaExists = await indexExists(METAINDEX,elasticservers)
    if (!(metaExists === true)) {
      const createdMeta = await put2Elastic(JSON.stringify({settings:{"index.mapping.ignore_malformed": true,"index.mapping.total_fields.limit":3000,"number_of_replicas":0, "index.refresh_interval": "10s"}}),METAINDEX,elasticservers)

    }
    const findingsExists = await indexExists(FINDINGSINDEX,elasticservers)
    if (!(findingsExists === true)) {
      const createdFindings = await put2Elastic(JSON.stringify({settings:{"index.mapping.ignore_malformed": true,"number_of_replicas":0, "index.refresh_interval": "10s"}}),FINDINGSINDEX,elasticservers)
    }


    // ok to proccess
    const dir = join(archiveDirectory,contentHash)
    const contentFile = join(dir,contentHash)
    //debug(contentFile)
    const files = []
    const ff = []
    let isnew = false
    if (!ismd5(contentHash)) return {contentHash,isnew,ff}
    // isdir ?
    for (const f of readdirSync(dir)) {
      if (f.endsWith('.json')) {

        const filename = join(dir,f)
        debug('filename',filename)
         try {
           const id = await digestFile(filename)
           const exists = await docExistInElastic(id,FILESINDEX,elasticservers)
           if (exists === false) {
             // TODO delete old journals
             // deleteDocs({journalname:filename},'files',elasticserver)
             const doc = {}
             doc.journalname = filename
             doc.journal = cleanup(JSON.parse(readFileSync(filename)))
             //debug('cleaned raw',raw)
             //if (Array.isArray(raw)) doc.journal = raw
             //else doc.journal = [ raw ]
             await putDoc2Elastic(JSON.stringify(doc),id,FILESINDEX,elasticservers)
             ff.push({isnew:true,id})
           } else {
             ff.push({isnew:false,id})
             //const old = await getDoc(id,FILESINDEX,elasticserver)
             //debug('old',old, contentHash === old.contentHash)

           }
         } catch (e) {
           debug('ERROR',e)
         }
      }
    }

    const exists = await docExistInElastic(contentHash,CONTENTINDEX,elasticservers)
    //debug('exists',exists)
    if (exists === false ) {
      isnew = true
      //debug('wait for tika')
      let rmeta = await tikaRMeta(contentFile,tikaservers)
      let content = rmeta.content
      delete rmeta.content
      const attachments = rmeta.attachments
      //delete rmeta.attachments
      if (attachments) {
        for (const attachment  of attachments) {
          if (attachment.content && attachment.content.length) {
            //console.dir(attachment.content)
            content += '\n' + attachment.content
            delete attachment.content
            debug('attachment',contentHash,attachment)
          }
        }
      }
      //debug(content)
      await putDoc2Elastic(JSON.stringify(rmeta),contentHash,METAINDEX,elasticservers)
      if (content && content.length) {
        let findigs
        if (content.length && content.length > MINCONTENTLENGTH2FIND) findigs = await pprocess(content)
        debug('findigs',findigs)
        if (findigs && Object.keys(findigs).length>0) {
          //debug(Object.keys(findigs).length)
          const ff = await putDoc2Elastic(JSON.stringify(findigs),contentHash,FINDINGSINDEX,elasticservers)
          //debug(ff)
        }
        const ssdeep = await hash_from_str(content)
        debug('ssdeep',ssdeep)
        const {chunksize, chunk, double_chunk } = hash_split(ssdeep)

        const r = await putDoc2Elastic(JSON.stringify({content,ssdeep,chunksize, chunk, double_chunk}),contentHash,CONTENTINDEX,elasticservers)

      } else {
        debug('no content',contentHash)
        const r = await putDoc2Elastic(JSON.stringify({}),contentHash,CONTENTINDEX,elasticservers)

      }

    }
    return {contentHash,isnew,ff}

}
