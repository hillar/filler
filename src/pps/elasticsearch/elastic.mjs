import  { default as createDebug }  from 'debug'
const debug = createDebug('elastic')

import { request } from 'http'

const ELASTICSERVER = 'localhost'
const ELASTICPORT = 9200
const ELASTICSERVERS = [{hostname:ELASTICSERVER,port:ELASTICPORT}]


// https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-exists.html
export function indexExists(index,servers=ELASTICSERVERS) {
  return new Promise ( resolve => {
    // TODO roundrobin and query another if fails
    const hostname = servers[0].hostname
    const port = servers[0].port
    const options = {
      hostname,
      port,
      path: index,
      method: 'HEAD'
    }
    const reqHead = request(options, r => {
      debug(index,hostname,port,`HEAD statusCode: ${r.statusCode}`)
      resolve(r.statusCode === 200)
    })
    reqHead.end()
  })
}

/*
curl -s -XPUT -H 'Content-Type: application/json' "http://aju2:9200/content-127.0.0.1_7200" -d'{
*/
export function put2Elastic(doc,path,servers=ELASTICSERVERS){
  return new Promise ( resolve => {
    // TODO roundrobin and query another if fails
    const hostname = servers[0].hostname
    const port = servers[0].port

    debug('put2Elastic',path,hostname,port)
    const options = {
      hostname: hostname,
      port: port,
      path: `${path}`,
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      debug(path,`PUT statusCode: ${res.statusCode}`)
      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(id)
          console.error(e)
          resolve()
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          if (res.statusCode > 299) {
            console.error(path,hostname,port)
            console.error(r)
            console.dir(doc)
          }
          debug(path,hostname,port,r)
          resolve(r)

        } catch (e) {
          debug(path,hostname,port,e)
          resolve()
        }
      })
    })
    req.on('error', (error) => {
      debug(error)
      resolve()
    })
    req.write(doc,'utf-8')
    req.end()
  })
}


/*

POST twitter/_delete_by_query
{
  "query": {
    "match": {
      "message": "some message"
    }
  }
}

*/

/*

curl -XPUT "localhost:9200/your_example_index/_settings" -H 'Content-Type: application/json' -d'
{
    "index" : {
        "index.highlight.max_analyzed_offset" : 100000
    }
}

*/

export function putDoc2Elastic(doc,id,index,servers=ELASTICSERVERS){
  //return new Promise( (resolve) => {
    const path = `/${index}/_doc/${id}`
    debug('putDoc2Elastic',index,id)
    return put2Elastic(doc,path,servers)
    /*
    const options = {
      hostname: server,
      port: port,
      path: path,
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      debug(id,index,`PUT statusCode: ${res.statusCode}`)
      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(id)
          console.error(e)
          resolve()
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          if (res.statusCode > 299) {
            console.error(id)
            console.error(r)
            console.dir(doc)
          }
          debug(id,r)
          resolve(r)

        } catch (e) {
          debug(id,e)
          resolve()
        }
      })
    })
    req.on('error', (error) => {
      debug(error)
      resolve()
    })
    req.write(doc,'utf-8')
    req.end()
    */

  //})
}

export function docExistInElastic(id,index,servers=ELASTICSERVERS){
  return new Promise( (resolve) => {
    // TODO roundrobin and query another if fails
    const hostname = servers[0].hostname
    const port = servers[0].port
    const options = {
      hostname: hostname,
      port: port,
      path: `/${index}/_doc/${id}`,
      method: 'HEAD'
    }
    const req = request(options, (res) => {
      debug(id,index,`EXISTS statusCode: ${res.statusCode}`)
      resolve( (res.statusCode === 200) )
    })
    req.on('error', (error) => {
      debug(error)
      resolve()
    })
    req.end()
  })
}


function _getDoc(id,fields,index,server,port){
  return new Promise( (resolve) => {
    //curl aju2:9200/content/_doc/535171de5b38dd14c3b3d39cd1b09c6d/_source?_source_includes=*
    const options = {
      hostname: server,
      port: port,
      path: `/${index}/_doc/${id}/_source?_source_includes=${fields}`,
      method: 'GET'
    }
    const req = request(options, (res) => {
      debug(id,index,`getDoc statusCode: ${res.statusCode}`)

      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(e)
          console.error(query)
          resolve({})
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          debug(r)
          //debug(r._id, (r._id === id))
          //if (r._id === id) resolve(r)
          //else resolve({})
          resolve(r)
        } catch (e) {
          debug(e)
          resolve({})
        }
      })

    })
    req.on('error', (error) => {
      debug(error)
      resolve()
    })
    req.end()
  })
}

export function getFieldFromDoc(id,field,index,servers=ELASTICSERVERS){
  // TODO roundrobin and query another if fails
  const hostname = servers[0].hostname
  const port = servers[0].port
  return _getDoc(id,field,index,hostname,port)
}
export function getDoc(id,index,servers=ELASTICSERVERS){
  // TODO roundrobin and query another if fails
  const hostname = servers[0].hostname
  const port = servers[0].port
  return _getDoc(id,'*',index,hostname,port)
}


/*

Elasticsearch supports the following special characters in query string:
– + : AND operation
– | : OR operation
– - : negates a single token
– " : wraps a number of tokens to signify a phrase for searching (e.g: “java sample approach”)
– * : at the end of a term to signigy a prefix query (e.g: java*)
– ( and ) : signify precedence

*/

export function elasticSimpleQuery(query,index,from,size,server=ELASTICSERVER,port=ELASTICPORT){
  return new Promise( (resolve) => {
    const qq = {
          "query": {
            "simple_query_string" : {
                "query": query,
                //"fields": [field],
                "default_operator": "and"
            },
          },
          "highlight": {
                  "fields": {
                      "*": {"require_field_match": false}
                  }
          },
          "_source": false,
          "sort": ["_score"],
          "from": from,
          "size": size
        }

    debug(JSON.stringify(qq))
    const options = {
      hostname: server,
      port: port,
      path: `/${index}/_search`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      debug(query,index,`elasticSimpleQuery statusCode: ${res.statusCode}`)
      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(e)
          console.error(query)
          resolve({})
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          debug(r)
          debug(r.hits)
          if (r.hits)resolve(r.hits)
          else resolve({})

        } catch (e) {
          debug(e)
          resolve({})
        }
      })
    })
    req.on('error', (error) => {
      debug(error)
      resolve({})
    })

    req.write(JSON.stringify(qq))
    req.end()
  })
}

// curl 'aju2:9200/files/_search?q=contentHash:bad00bc4c0b1f1f00c648ef81bd016d1'

export function elasticQuery(query,field,index,server=ELASTICSERVER,port=ELASTICPORT){
  return new Promise( (resolve) => {

    const options = {
      hostname: server,
      port: port,
      path: `/${index}/_search?q=${field}:${query}&df=_index`,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      debug(`${server}:${port}/${index}/_search?q=${field}:${query}`)
      debug(query,index,`elasticQuery statusCode: ${res.statusCode}`)
      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(e)
          console.error(query)
          resolve({})
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          debug(r)
          debug(r.hits)
          if (r.hits)resolve(r.hits)
          else resolve({})

        } catch (e) {
          debug(e)
          resolve({})
        }
      })
    })
    req.on('error', (error) => {
      debug(error)
      resolve({})
    })

    req.end()
  })
}
