import  { default as createDebug }  from 'debug'
const debug = createDebug('est-id')

export function isEstonianIDCode(code) {
  function getBirthday(code) {
    let year = parseInt(code.substring(1, 3))
    const month = parseInt(code.substring(3, 5).replace(/^0/, '')) - 1
    const day = code.substring(5, 7).replace(/^0/, '')
    const firstNumber = code.charAt(0)
    if (firstNumber === '1' || firstNumber === '2') {
      year += 1800;
    } else if (firstNumber === '3' || firstNumber === '4') {
      year += 1900;
    } else if (firstNumber === '5' || firstNumber === '6') {
      year += 2000;
    } else {
      year += 2100;
    }
    return new Date(year, month, day);
  }
  function getControlNumber(code) {
    let multiplier1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 1],
      multiplier2 = [3, 4, 5, 6, 7, 8, 9, 1, 2, 3],
      mod,
      total = 0;
    for (let i = 0; i < 10; i++) {
      total += code.charAt(i) * multiplier1[i];
    }
    mod = total % 11;
    total = 0;
    if (10 === mod) {
      for (let i = 0; i < 10; i++) {
        total += code.charAt(i) * multiplier2[i];
      }
      mod = total % 11;
      if (10 === mod) {
        mod = 0;
      }
    }
    return mod;
  }
  if (code.length !== 11) {
    return false
  }
  const control = getControlNumber(code);
  if (control !== parseInt(code.charAt(10))) {
    return false;
  }
  debug(code)
  const year = Number(code.substr(1, 2));
  const month = Number(code.substr(3, 2));
  const day = Number(code.substr(5, 2));
  const birthDate = getBirthday(code);
  return year === birthDate.getFullYear() % 100 && birthDate.getMonth() + 1 === month && day === birthDate.getDate();
}
