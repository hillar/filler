import  { default as createDebug }  from 'debug'
const debug = createDebug('search')

import { createServer } from 'http'
import { createReadStream } from 'fs'
import { join } from 'path'
import { elasticSimpleQuery, elasticQuery, getDoc } from '../elasticsearch/elastic'

const ELASTICSERVER = 'aju1'
const CONTENTINDEX = 'content-*'
//const field = 'content'
const contentHash = 'journal.ondisk.contentHash'
const FILESINDEX = 'files-*'
const METAINDEX = 'meta-*'

const ARCHIVEDIR = '/hdd/sdd/ain/archive/'

const MAXPREVIEWSIZE = 1024 * 10

async function search (query, start, size) {
  const sr = await elasticSimpleQuery(query,CONTENTINDEX,start,size,ELASTICSERVER)
  if (sr && sr.hits) {
    for (const srhit of sr.hits){
      debug('search',srhit._id)
      const fr = await elasticQuery(srhit._id,contentHash,FILESINDEX,ELASTICSERVER)
      if (fr.hits) {
        srhit.files = []
        for (const frhit of fr.hits ){
          debug('frhit',frhit)
          //srhit.files.push({filename:frhit._source.filename,host:frhit._source.hostname,_id:frhit._id})
          //
          const r = frhit._source.journal.ondisk
          r.fields = frhit._source.journal.fields
          srhit.files.push(r)
        }
      }
    }
  }
  return sr
}








(async () => {
    try {

        //const sr = await search('Trandlex',0,20)
        //console.log(JSON.stringify(sr,null,2))
        const server = createServer( async (req, res) => {
          debug('req.url',req.url)
          if (req.method === 'GET'){

            if (req.url.startsWith('/?q=')){
                const params = req.url.substring(2).split('&')
                const p = {}
                p.start = 0
                p.size = 3
                for (const param of params) {
                  let kv = param.split('=')
                  debug(kv)
                  p[kv[0]]= kv[1].trim()
                }
                //console.dir(p)
                if (p.q && p.q.length && p.q.length > 0) {
                  const sr = await search(decodeURIComponent(p.q),p.start,p.size)
                  res.write(JSON.stringify(sr,null,2))
                  res.end();
                  //console.dir(sr)
                } else {
                  res.writeHead(404)
                  res.end();
                }

            } else if (req.url.startsWith('/?get=')){
              const id = req.url.split('=')[1]
              const sr = await getDoc(id,index,ELASTICSERVER)
              res.write(JSON.stringify(sr))
              res.end()

            } else if (req.url.startsWith('/?preview=')){
              const id = req.url.split('=')[1]
              const i = await elasticQuery(id,'_id',CONTENTINDEX,ELASTICSERVER)
              if (i && i.hits && i.hits[0] && i.hits[0]._index) {
                const sr = await getDoc(id,i.hits[0]._index,ELASTICSERVER)
                debug('preview',sr)
                if (sr && sr.content) {
                  res.write(JSON.stringify({content:sr.content.substring(0,MAXPREVIEWSIZE)}))
                } else res.writeHead(404)
              } else res.writeHead(404)
              res.end()

            } else if (req.url.startsWith('/?meta=')){
              const id = req.url.split('=')[1]
              const i = await elasticQuery(id,'_id',METAINDEX,ELASTICSERVER)
              debug('meta index',i)
              if (i && i.hits && i.hits[0] && i.hits[0]._index) {
                const sr = await getDoc(id,i.hits[0]._index,ELASTICSERVER)
                debug('meta res',sr)
                if (sr) res.write(JSON.stringify({tika:sr}))
                else res.writeHead(404)
              } else res.writeHead(404)
              res.end()

            } else if (req.url.startsWith('/?download=')){
              const ids = req.url.split('=')[1]
              /*
              const contentHash = ids.split('/')[0]
              const nameHash = ids.split('/')[1]
              const sr = await elasticQuery(contentHash,'_id','files',ELASTICSERVER)
              //console.log(sr.hits[0])
              const contentHash = sr.hits[0]._source.contentHash
              const filename = sr.hits[0]._source.filename.replace("\\",'_').replace("\\",'_').replace('/','_').replace(':','_')
              //console.log(contentHash,filename)
              const mr = await elasticQuery(contentHash,'_id','meta',ELASTICSERVER)
              let ct
              try {
                ct = mr.hits[0]._source['Content-Type']
              } catch(e) {
                ct = ''
              }
              res.setHeader('Content-disposition', 'attachment; filename='+filename);
              //res.write('\n\nnot implemented (yet)  :'+ct+'  '+filename)
              console.log(join(ARCHIVEDIR,contentHash,contentHash))
              const rs = createReadStream(join(ARCHIVEDIR,contentHash,contentHash))
              rs.pipe(res)
              //console.log(JSON.stringify(mr,null, 2))
              */

            } else {

            }

          } else {
            res.writeHead(404)
            res.end();
          }

        });
        server.on('clientError', (err, socket) => {
          socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
        });
        server.listen(8765,'172.16.4.1');

    } catch (e) {
      console.error(e)
    }
})()
