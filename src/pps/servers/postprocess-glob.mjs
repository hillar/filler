import  { default as createDebug }  from 'debug'
const debug = createDebug('glob')

import { readdirSync} from 'fs'
import { processDir } from '../process/process'
import { Command } from '../../vendor/commander'


(async () => {

  const cliParams = new Command()
  const tripleParse = function (m) {
    const hostname = m.split(':')[0].trim()
    if (!hostname) throw new Error('no hostname')
    let port
    try {
      port = m.split(':')[1].split('/')[0]
    } catch (e) {
      throw new Error('no port and no suffix')
    }
    port = Number(port)
    if (!port) throw new Error('port not number')
    const suffix = m.split('/')[1]
    if (!suffix)  {
      throw new Error('no suffix')
    }
    return {hostname,port,suffix}
  }
  const hostportParse = function (m){
    const hostname = m.split(':')[0].trim()
    if (!hostname) throw new Error('no hostname')
    let port
    try {
      port = m.split(':')[1]
    } catch (e) {
      throw new Error('no port')
    }
    port = Number(port)
    if (!port) throw new Error('port not number')
    if (port > 65535) throw new Error('port should be >= 0 and < 65536')
    return {hostname,port}

  }
  cliParams
     .version('0.0.0')
     .usage('[options]')
     .option('-f --filespath <path>',`path to read files`)
     .option('-s --suffix <string>',`suffix for elasticsearch indexes`)
     .option('-e --elasticservers <list>','list of comma separated host:port pairs (sample 127.0.0.1:9200)', list => list.split(',').map(m=>hostportParse(m)))
     .option('-t --tikaservers <list>','list of comma separated host:port pairs (sample 127.0.0.1:9998)', list => list.split(',').map(m=>hostportParse(m)))
  try {
  cliParams.parse(process.argv)
} catch (e) {
  console.error(e)
  process.exit(-1)
}
  debug('path',cliParams.filespath)
  debug('elasticservers',cliParams.elasticservers)
    let count = 0
    try {
      for (const dirname of readdirSync(cliParams.filespath)) {
        //if (count > 5) break
        count ++
        let ff = await processDir(dirname,cliParams.filespath,cliParams.tikaservers,cliParams.elasticservers,cliParams.suffix)
        //ff = await processHash('00601c30b986ae4b71f292e8b56a02df',ARCHIVEDIRECTORY,TIKASERVER,ELASTICSERVER,'aju1_7200')
        //debug(f,ff)
        if (ff.isnew === true) debug(ff)
        if ((count % 100) === 0) console.log(count)
      }
    } catch (e) {
      console.error(e)
    }
})()
