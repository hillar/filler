import  { default as createDebug }  from 'debug'
const debug = createDebug('glob')

import { readdirSync, readFileSync, watch } from 'fs'
import { join } from 'path'
/*
import { tikaRMeta } from '../tika/tika'
import { putDoc2Elastic, docExistInElastic } from '../elasticsearch/elastic'
import { isEstonianIDCode } from '../parsers/est-id'
import { hash_from_str, hash_split } from '../hash/ssdeep'
*/
import { processHash } from '../process-single'

const TIKASERVER = 'localhost'
const ARCHIVEDIRECTORY = '/hdd/sdc/7300'
const ELASTICSERVER = 'aju2'
const ELASTICPORT = 9200
const MINCONTENTLENGTH2FIND = 11


/*

curl -XPUT 'aju2:9200/meta/_settings' -H 'Content-Type: application/json' -d'
{
"index" : {
"mapping" : {
"total_fields" : {
"limit" : "100000"
}
}
}
}'


*/

/*
function pprocess(txt){
  return new Promise( (resolve) => {
    if (!txt) { resolve(); return}
    let numbers
    try {
      numbers = txt.match(/\d+/g).map(Number)
    } catch (e) {
      resolve({})
      return
    }
    debug(numbers.length)
    const isikukoodid = []
    for (const n of numbers) {
      try {
      if (isEstonianIDCode(n+'')) isikukoodid.push(n+'')
    } catch (e) {
      debug (e)
    }
    }
    debug('isik',isikukoodid[0])
    resolve({isikukoodid})
  })
}

function getMetaFiles(dir){
  const metaFiles = []
  const files = readdirSync(dir)
  for (const f of files) {
    if (f.endsWith('.json')) metaFiles.push(f)
  }
  return metaFiles
}

function preprocess(contentHash,archiveDirectory=ARCHIVEDIRECTORY, tikaserver=TIKASERVER, elasticserver=ELASTICSERVER){
  return new Promise( async(resolve) => {
    const dir = join(archiveDirectory,contentHash)
    const contentFile = join(dir,contentHash)
    //debug(contentFile)
    const files = []
    const ff = []
    let isnew = false
    for (const f of readdirSync(dir)) {
      if (f.endsWith('.json')) {
         try {
           const doc = readFileSync(join(dir,f))
           files.push(JSON.parse(doc))
           const id = f.split('.')[0]

           const exists = await docExistInElastic(id,'files',elasticserver)
           if (exists === false) {
             await putDoc2Elastic(doc,id,'files',elasticserver)
             ff.push({isnew:true,id})
           } else ff.push({isnew:false,id})
         } catch (e) {
           debug(e)
         }
      }
    }

    const exists = await docExistInElastic(contentHash,'content',elasticserver)
    if (exists === false ) {
      isnew = true
      debug('wait for tika')
      let rmeta = await tikaRMeta(contentFile,tikaserver)
      const content = rmeta.content
      delete rmeta.content
      await putDoc2Elastic(JSON.stringify(rmeta),contentHash,'meta',elasticserver)
      if (content && content.length) {
        let findigs
        if (content.length && content.length > MINCONTENTLENGTH2FIND) findigs = await pprocess(content)
        if (findigs) await putDoc2Elastic(JSON.stringify(findigs),contentHash,'findigs',elasticserver)
        const ssdeep = await hash_from_str(content)
        debug(ssdeep)
        const {chunksize, chunk, double_chunk } = hash_split(ssdeep)

        const r = await putDoc2Elastic(JSON.stringify({content,ssdeep,chunksize, chunk, double_chunk}),contentHash,'content',elasticserver)

      } else {
        debug('no content',contentHash)
        const r = await putDoc2Elastic(JSON.stringify({}),contentHash,'content',elasticserver)

      }
    }
    resolve({contentHash,isnew,ff})

  })
}


function glob() {
  return new Promise( async (resolve) => {
    let count = 0
    const dir = '/hdd/sdd/ain/archive/'
    for (const f of readdirSync(dir)) {
      //if (count > 3) break
      count ++
      //if (count > 9800) console.log(f)
      let ff = await preprocess(f,ARCHIVEDIRECTORY,'aju2')
      debug(f,ff)
      if (ff.isnew === true) debug(ff)
      if ((count % 100) === 0) console.log(count)
      //if (count > 9800) console.log(f)
    }
    resolve()
  })
}


const POLLINTERVAL = 1000 * 10
let checkerTimeout
async function checker() {
    if (checkerTimeout) return
    let counter = 0
    let start = queue.length
    errors = {}
    while (queue.length > 0) {

    }
    checkerTimeout = setTimeout(function() {
        checkerTimeout = null
        checker()
    }, POLLINTERVAL)
}

*/

(async () => {
    const queue = []

    watch(ARCHIVEDIRECTORY ,(eventType, filename) => {
      if ((/[a-fA-F0-9]{32}/).test(filename)) {
        if (!queue.includes(filename)){
          queue.push(filename)
          console.log(eventType, filename,queue.length)
        }
      }
    })

    const POLLINTERVAL = 1000 * 10
    let checkerTimeout
    async function checker() {
        if (checkerTimeout) return
        let counter = 0
        let start = queue.length
        //errors = {}
        while (queue.length > 0) {
          counter ++
          const current = queue.shift()
          let ff = await processHash(current,ARCHIVEDIRECTORY,'aju2','aju2')
          //let ff = await preprocess(current,ARCHIVEDIRECTORY,'aju2')
          console.log(ff,counter)
        }
        console.log('end',start,counter)
        checkerTimeout = setTimeout(function() {
            checkerTimeout = null
            checker()
        }, POLLINTERVAL)
    }

    try {
        checker()

    } catch (e) {
      console.error(e)
    }
})();
