import  { default as createDebug }  from 'debug'
const debug = createDebug('pps')

import { parentPort, workerData }  from 'worker_threads'

import { processHash } from './postprocess/process'

(async () => {

const queue = []

  debug('starting with',workerData)

  parentPort.on('message', (message) =>  {
  	      debug(message)
          if (Object.keys(message).includes('new')) {
            queue.push(message['new'])
          }
          debug(queue)
  })

  parentPort.on('close', () => { console.log('parent closed') })

    const POLLINTERVAL = 1000 * 10
    let checkerTimeout
    async function checker() {
        if (checkerTimeout) return
        let counter = 0
        let start = queue.length
        //errors = {}
        while (queue.length > 0) {
          counter ++
          const current = queue.shift()
          //let ff = await processHash(current,ARCHIVEDIRECTORY,'aju2','aju2')
          //let ff = await preprocess(current,ARCHIVEDIRECTORY,'aju2')
          console.log(ff,counter)
        }
        console.log('end',start,counter)
        checkerTimeout = setTimeout(function() {
            checkerTimeout = null
            checker()
        }, POLLINTERVAL)
    }

    try {
        checker()

    } catch (e) {
      console.error(e)
    }
})()
