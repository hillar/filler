
$remote_addr

$remote_port

$remote_user

add_header X-remote_addr-$host $remote_addr;

location  ~ "^/\?(.*)$" {
    proxy_pass   http://search;     
}
location  ~ "^/[0-9a-f]{32}(.*)$" {
    proxy_pass   http://files;     
}




----

upstream search_servers {
    least_conn;
    server search1;
    server search2;
}

upstream file_servers {
    least_conn;
    server files1;
    server files2;
    server files3;
}

map $request_method $upstream {
    default search_servers;
    HEAD file_servers;
    PUT file_servers;
    PATCH file_servers;

}

server {

    location / {

        proxy_pass http://$upstream;

    }
}
